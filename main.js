const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

function createBookElement(book){
	const li = document.createElement("li");
	li.textContent = `${book.author ? book.author + ' -' : ''} ${book.name}${book.price ? ' - ' + book.price + ' грн' : ''}`;
	return li;
}

function renderBookList(){
	const rootDiv = document.getElementById("root");
	const ul = document.createElement("ul");
	
	books.forEach(book => {
		try{
			if(!book.hasOwnProperty("author")){
				throw new Error('missiing "author" property');
			}
			if(!book.hasOwnProperty("name")){
				throw new Error('missing "name" property');
			}
			if(!book.hasOwnProperty("price")){
				throw new Error('missing "price" property');
			}
			const li = createBookElement(book);
			ul.appendChild(li);
		}catch(error){
			console.log(error.message);
		}
	});
	rootDiv.appendChild(ul);
}

renderBookList();